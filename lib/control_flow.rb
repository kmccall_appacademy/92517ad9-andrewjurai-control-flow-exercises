# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.map { |ch| ch unless ch.downcase == ch }.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.even?
    starting_index = (str.length / 2) - 1
    str[starting_index, 2]
  else
    starting_index = (str.length - 1 ) / 2
    str[starting_index]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  counter = 0
  str.each_char { |ch| counter += 1 if VOWELS.include?(ch)}
  counter
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  return num if num <= 0
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined = ""
  arr.each.with_index do |str, indx|
    joined += str
    joined += separator if indx < arr.length - 1
  end
  joined
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weirdcased = str.downcase.chars.map.with_index do |ch, indx|
    if even_character?(indx)
      ch.upcase
    else ch
    end
  end
  weirdcased.join
end

def even_character?(indx)
  indx.odd?
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)

  new_str = str.split.map do |word|
    if long_word?(word)
      word.reverse
    else word
    end
  end
  new_str.join(" ")
end

def long_word?(word)
  word.length >= 5
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)

  (1..n).map do |int|
    if multiple_of_3_and_5?(int)
      "fizzbuzz"
    elsif multiple_of_5?(int)
      "buzz"
    elsif multiple_of_3?(int)
      "fizz"
    else int
    end
  end
end

def multiple_of_3?(int)
  int % 3 == 0
end

def multiple_of_5?(int)
  int % 5 == 0
end

def multiple_of_3_and_5?(int)
  int % 3 == 0 && int % 5 == 0
end
# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  orig = arr.map { |int| int }
  reversed_arr = []

  orig.length.times { reversed_arr.push(orig.pop) }
  # orig.each.with_index do |int, indx|
  #   adjusted_index = orig.length - 1 - indx
  #   reversed_arr[adjusted_index] = int
  # end
  reversed_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return true if num == 2
  return false if num == 1
  (2...num).none? { |int| num % int == 0 }
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).map { |int| int if num % int == 0 }.compact
end

# Write a method that returns a sorted array of the prime factors of its arg.
def prime_factors(num)
  factors(num).map { |int| int if prime?(int) }.compact
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  majority = more_evens_or__more_odds?(arr)
  if majority == "more evens"
    return arr.select(&:odd?)[0]
  elsif majority == "more odds"
    return arr.select(&:even?)[0]
  end
end

def more_evens_or__more_odds?(arr)
  num_evens = arr.count(&:even?)
  num_odds = arr.count(&:odd?)
  if num_evens > num_odds
    "more evens"
  else "more odds"
  end
end
